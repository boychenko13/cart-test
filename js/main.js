productInit();

function productInit() {

    let cartBtn        = document.getElementById('cart'),
        inputCount     = document.getElementById('inputCount'),
        btn_plus       = document.getElementById('btn_plus'),
        btn_minus      = document.getElementById('btn_minus'),
        productName    = document.getElementById('productName'),
        productPrice   = document.getElementById('productPrice'),
        productDesc    = document.getElementById('productDescr'),
        setColor       = new Set(),
        setSize        = new Set(),
        count          = 1,
        cost           = 0,
        colorSelect    = document.getElementById('sel1'),
        sizeSelect     = document.getElementById('sel2'),
        productPanel   = document.querySelector('.product'),
        popWindow      = document.querySelector('.popup'),
        popCloseBtn    = document.querySelector('.close-btn'),
        minicartList   = document.querySelector('.minicart-list'),
        minicartTotal  = document.querySelector('.minicart__total'),
        minicartClose  =  document.querySelector('.minicart__close-btn'),
        cartWindow     = document.querySelector('.minicart');

    let options = [
        {
            name: "color",
            values:["red", "blue", "green", "black"]
        },
        {
            name: "size",
            values:["S", "M", "L"]
        },
    ]

    let product = {
        "variants": [
            {
                "id": "1",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Red dress, Size S",
                "option1": "red",
                "option2": "S",
                "options": ["red", "S"],
                "in_stock": 9,
                "price": 1000,
                "image": "red"
            }, {
                "id": "2",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Red dress, Size M",
                "option1": "red",
                "option2": "M",
                "options": ["red", "M"],
                "in_stock": 0,
                "price": 1000,
                "image": "red"
            }, {
                "id": "3",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Red dress, Size L",
                "option1": "red",
                "option2": "L",
                "options": ["red", "L"],
                "in_stock": 9,
                "price": 1000,
                "image": "red"
            }, {
                "id": "4",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Blue dress, Size S",
                "option1": "blue",
                "option2": "S",
                "options": ["blue", "S"],
                "in_stock": 2,
                "price": 120,
                "image": "blue"
            }, {
                "id": "5",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Black dress, Size S",
                "option1": "black",
                "option2": "S",
                "options": ["black", "S"],
                "in_stock": 1,
                "price": 100,
                "image": "black"
            },
            {
                "id": "6",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet accumsan tortor. Vestibulum mollis lobortis lorem, vel suscipit lectus fringilla ac. Aliquam fermentum, dui nec vehicula venenatis, diam purus sodales arcu, ut rutrum dolor nunc iaculis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh nunc, laoreet.",
                "title": "Black dress, Size M",
                "option1": "black",
                "option2": "M",
                "options": ["black", "M"],
                "in_stock": 1,
                "price": 105,
                "image": "black"
            }
        ]
    }

    inputCount.addEventListener('input', checkInputValue);
    popCloseBtn.addEventListener('click', () =>windowEvent("close","popup"));
    btn_plus.addEventListener('click', () => changeCount('+'));
    btn_minus.addEventListener('click', () =>changeCount('-'));
    minicartClose.addEventListener('click', () => windowEvent("close","cart"));
    avaliableColors();

    colorSelect.addEventListener('change',function() {
        if (colorSelect.value != 'empty') {
            createSizeSelector(colorSelect.value);
            cartBtn.innerText = 'Выберете размер';
            updateInfo(colorSelect.value, sizeSelect.value);
        }
    });


    sizeSelect.addEventListener('change',function(){
        if (sizeSelect.value != 'empty') {
            updateInfo(colorSelect.value, sizeSelect.value);
        }
    });

    cartBtn.addEventListener('click', function(event){
        event.preventDefault;
        selectItem(colorSelect.value, sizeSelect.value);
    });


    function addListenersToButtons(){
        cartWindow.addEventListener("click", function(event) {
            if(event.target.classList.contains('minicart__del-btn')){
                removeFromCart(event.target);
            }
        });
    }

    function filterProductImage(color) {
        $('.productSlider').slick('slickFilter', `.${color}`);
    }

    function changeCount(operation){

        if (operation == '+') {
            count++;
            inputCount.value = count;
        }

        else if (operation == '-'){
            if (count != 1){
                count--;
                inputCount.value = count;
            }
        }

        else {
            count = 1;
            inputCount.value = count;
        }
    }

    function windowEvent(event,window) {
        if (event =="close" && window == "popup"){
            popWindow.style.display = 'none';
        }
        if (event =="open" && window == "popup"){
            popWindow.style.display = 'block';
        }
        if (event =="close" && window == "cart"){
            cartWindow.style.display = 'none';
        }
        if (event =="open" && window == "cart"){
            cartWindow.style.display = 'block';
        }
    }

    function checkInputValue() {
        let value = inputCount.value;

        if (value != '') {
            value = parseInt(inputCount.value, 10);
            if (isNaN(value)){
                value = 1;
            }
            count = value;
            inputCount.value = count;
        }
        else {
            count = 1;
            inputCount.value = count;
        }
    }

    function createSizeSelector(color){
        sizeSelect.innerHTML = '';
        setSize.clear();
        for (let i = 0; i < product.variants.length; i++){
            if (color === product.variants[i].option1){
                setSize.add(product.variants[i].option2);
            }
        }
        setSize.forEach( sizes => {
            let option = document.createElement('option');
            option.value = sizes;
            option.innerText = sizes;
            sizeSelect.appendChild(option);

        });
    }


    function avaliableColors(){
        for(let i = 0; i < product.variants.length; i++){
            setColor.add(product.variants[i].option1);
        }

        setColor.forEach( colors => {
            let option = document.createElement('option');
            option.value = colors;
            option.innerText = colors;
            colorSelect.appendChild(option);
        });
    }


    function addToCart(id, qty, price){
       minicartTotal.innerText = cost += price * qty;

       if (qty != 0){
           for(let i = 0; i < product.variants.length; i++) {
               if(product.variants[i].id == id ) {
                   let li = document.createElement('li');
                   li.classList.add('minicart-list__element');
                   li.classList.add(product.variants[i].image);
                   li.innerHTML = '<div>' + product.variants[i].title + '</div>' +
                       '<div>' + 'Цена: ' + '<span class="price">' +product.variants[i].price + '$'+'</span>' +'</div>' +
                       '<div>' + 'Количество: ' +'<span class="qty">' +qty+ '</span>' +'</div>' +
                       '<button class="minicart__del-btn" data-prodId = "' + product.variants[i].id + '" class="removeBtn">' + 'Удалить' + '</button>';
                   minicartList.appendChild(li);
                   addListenersToButtons();
                   windowEvent("open","cart");
               }
           }
       }

       else {
           windowEvent("open", "popup");
       }
    }


    function removeFromCart(btn){
        let product = btn.parentNode;
        let list = product.parentNode;
        list.removeChild(product);
        let prodPrice = parseInt(product.querySelector('.price').innerText);
        let prodQty   = parseInt(product.querySelector('.qty').innerText);
        minicartTotal.innerText = cost -= prodPrice*prodQty;
    }


    function updateInfo(color, size){
        if (productPanel.classList.contains('no-product')) {
            productPanel.classList.toggle('no-product');
            let productSlider = document.querySelector('.productSlider');
            productSlider.classList.toggle('no-slider');

        }
        if (colorSelect.value != 'empty' && sizeSelect.value != 'empty'){
            for(let i = 0; i < product.variants.length; i++) {
                if(color ===  product.variants[i].option1 && size ===  product.variants[i].option2){
                    productName.innerText = product.variants[i].title;
                    productPrice.innerText = product.variants[i].price + "$";
                    productDesc.innerText =  product.variants[i].description;
                    $('.productSlider').slick('slickUnfilter');
                    filterProductImage(product.variants[i].image);
                    cartBtn.innerText = 'Добавить в корзину';
                    changeCount()
                }
            }
        }
    }


    function selectItem(color, size) {
        for(let i = 0; i < product.variants.length; i++) {
            if(color ===  product.variants[i].option1 && size ===  product.variants[i].option2){
                if (count <= product.variants[i].in_stock){
                    addToCart(product.variants[i].id,count, product.variants[i].price);
                }
                else {
                    popWindow.querySelector('.additionalInfo').innerText = 'Товар не добавлен в полном объеме!';
                    windowEvent('open','popup');
                    addToCart(product.variants[i].id, product.variants[i].in_stock, product.variants[i].price);
                }
            }
        }
    }
}